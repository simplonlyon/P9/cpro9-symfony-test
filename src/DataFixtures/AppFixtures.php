<?php

namespace App\DataFixtures;

use App\Entity\Dog;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for($x = 1; $x < 5; $x++) {
            $user = new User();
            $user->setEmail('mail'.$x.'@mail.com');
            $user->setPassword('$argon2id$v=19$m=65536,t=4,p=1$WWhsWjRmVlMyZXJjVGxuZg$U4TULUJkJSggrhagNSVN2PAncKdpKzz5x67BjH/oMso'); //pass: 1234

            $dog = new Dog();
            $dog->setName("Dog" .$x);
            $dog->setBreed("Breed".$x);
            $dog->setBirthdate(new DateTime());

            $manager->persist($user);
            $manager->persist($dog);

        }


        $manager->flush();
    }
}
