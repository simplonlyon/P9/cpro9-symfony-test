<?php

namespace App\Tests\Api;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;

class UserApiTest extends ApiTestCase
{
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var UserRepository
     */
    private $repo;

    public function setUp()
    {
        $kernel = self::bootKernel();

        $this->em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $this->repo = $this->em
            ->getRepository(User::class);

    }

    public function testRegister()
    {
        $client = static::createClient();
        $crawler = $client->request('POST', '/api/users', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'email' => 'test@test.com',
                'password' => 'test'
            ]
        ]);

        $this->assertResponseIsSuccessful();

        $user = $this->repo->findOneBy(['email' => 'test@test.com']);
        $this->assertNotNull($user);
        $this->assertNotEquals('test', $user->getPassword());
    }

    public function testGetJWT()
    {
        $client = static::createClient();
        $crawler = $client->request('POST', '/api/authentication_token', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'email' => 'mail1@mail.com',
                'password' => '1234'
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $json = json_decode($crawler->getContent(), true);
        $this->assertNotNull($json['token']);

        return $json['token'];
    }

    public function testGetUser()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/api/users/1');

        $this->assertResponseIsSuccessful();
        $json = json_decode($crawler->getContent(), true);
        $this->assertEquals('mail1@mail.com', $json['email']);
        $this->assertArrayNotHasKey('password', $json);
    }

    public function testRemoveUser()
    {
        $client = static::createClient();
        $crawler = $client->request('DELETE', '/api/users/1', [
            'headers' => ['Authorization' => 'Bearer '.$this->getToken()]
        ]);
        
        $this->assertResponseIsSuccessful();
        
        $users = $this->repo->findAll();
        $this->assertCount(3, $users);
    }
    
    public function testRemoveUserUnauthorized()
    {
        $client = static::createClient();
        $crawler = $client->request('DELETE', '/api/users/2', [
            'headers' => ['Authorization' => 'Bearer '.$this->getToken()]
        ]);
        
        $this->assertResponseStatusCodeSame(403);
        
        $users = $this->repo->findAll();
        $this->assertCount(4, $users);
    }

    public function testPatchUser()
    {
        $client = static::createClient();
        $crawler = $client->request('PATCH', '/api/users/1', [
            'headers' => ['Authorization' => 'Bearer '.$this->getToken(), 'Content-Type' => 'application/json'],
            'json' => [
                'email' => 'test@test.com'
            ]
        ]);
        
        $this->assertResponseIsSuccessful();
        $json = json_decode($crawler->getContent(), true);
        $this->assertEquals('test@test.com', $json['email']);
    }

    private function getToken() {
        
        $jwtManager = self::bootKernel()->getContainer()->get('lexik_jwt_authentication.jwt_manager');
        $user = $this->repo->find(1);
        return $jwtManager->create($user);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->em->close();
        $this->em = null;
        $this->repo = null;
    }
}
