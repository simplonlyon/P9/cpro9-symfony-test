<?php

namespace App\Tests\Api;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Dog;
use App\Repository\DogRepository;
use Doctrine\ORM\EntityManager;

class DogApiTest extends ApiTestCase
{


    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var DogRepository
     */
    private $repo;

    public function setUp() {
        $kernel = self::bootKernel();

        $this->em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $this->repo = $this->em
            ->getRepository(Dog::class);
    }

    public function testFetchDogs($count = 4)
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/api/dogs');

        $this->assertResponseIsSuccessful();
        $this->assertMatchesResourceCollectionJsonSchema(Dog::class);
        
        $json = json_decode($crawler->getContent(), true);
        $this->assertEquals($count, $json['hydra:totalItems']);
        $this->assertCount($count, $json['hydra:member']);
    }

    public function testFetchOneDog()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/api/dogs/1');

        $this->assertResponseIsSuccessful();
        $this->assertMatchesResourceItemJsonSchema(Dog::class);
        
        $json = json_decode($crawler->getContent(), true);
        $this->assertEquals("Dog1", $json['name']);
    }

    
    public function testPostDogs()
    {
        $client = static::createClient();
        $crawler = $client->request('POST', '/api/dogs', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'name' => 'test',
                'breed' => 'testbreed',
                'birthdate' => '2020-01-01'
            ]
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertMatchesResourceCollectionJsonSchema(Dog::class);
        
        $json = json_decode($crawler->getContent(), true);
        $this->assertNotNull($json['id']);
        $this->assertCount(5, $this->repo->findAll());
        // $this->testFetchDogs(5); // Dépendance à un autre test, pas ouf pasque si l'autre test marche pas alors que celui ci marche, ça plantera et du coup on sait pas d'où vient le problème
        //Il serait préférable de faire un count en utilisant le repository
        // $this->assertCount(4, $json['hydra:member']);
    }

    public function testPatchDogs()
    {
        $client = static::createClient();
        $crawler = $client->request('PATCH', '/api/dogs/1', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'name' => 'test'
            ]
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertMatchesResourceCollectionJsonSchema(Dog::class);
        
       
        $dog = $this->repo->find(1);
        
        $this->assertEquals('test', $dog->getName());
        $this->assertEquals('Breed1', $dog->getBreed());
    }

    public function testDeleteDogs()
    {
        $client = static::createClient();
        $crawler = $client->request('DELETE', '/api/dogs/1');

        $this->assertResponseIsSuccessful();
        
        
        $this->assertCount(3, $this->repo->findAll());
    }


    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->em->close();
        $this->em = null;
        $this->repo = null;
    }
    
}
