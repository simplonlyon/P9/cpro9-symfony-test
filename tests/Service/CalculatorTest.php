<?php

namespace App\Tests\Service;

use App\Service\Calculator;
use Exception;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{
    /**
     * @var Calculator
     */
    private $calculator;
    public function setUp(): void {
        $this->calculator = new Calculator();
    }

    public function testAddSuccess()
    {
        $a = 2;
        $b = 4;
        $expected = 6;
        $result = $this->calculator->add($a,$b);
        
        $this->assertEquals($expected, $result);
    }

    public function testSubstractSuccess()
    {
        $a = 2;
        $b = 4;
        $expected = -2;
        $result = $this->calculator->substract($a,$b);
        
        $this->assertEquals($expected, $result);
    }
    public function testMultiplySuccess()
    {
        $a = 2;
        $b = 4;
        $expected = 8;
        $result = $this->calculator->multiply($a,$b);
        
        $this->assertEquals($expected, $result);
    }
    
    public function testDivideSuccess()
    {
        $a = 4;
        $b = 2;
        $expected = 2;
        $result = $this->calculator->divide($a,$b);
        
        $this->assertEquals($expected, $result);
    }
    
    public function testDivideByZeroRaiseExpection()
    {
        $a = 4;
        $b = 0;
        try {
            $this->calculator->divide($a,$b);
            
        }catch(Exception $e) {
            $this->assertInstanceOf(Exception::class, $e);
        }
        
    }

    public function testIsEvenWithEvenNumber() {
        $number = 4;
        
        $actual = $this->calculator->isEven($number);

        $this->assertTrue($actual);
    }
    
    public function testIsEvenWithOddNumber() {
        $number = 3;
        
        $actual = $this->calculator->isEven($number);

        $this->assertFalse($actual);
    }
    
    public function testIsEvenWithString() {
        $number = "Bloup";
        try {
            $this->calculator->isEven($number);
            $this->fail('Expected exception');
        } catch(InvalidArgumentException $e) {
            $this->assertInstanceOf(InvalidArgumentException::class, $e);
        }
    }
    
    //Si jamais on a un throw dans notre méthode 
    // public function testDivideByZeroRaiseExpection()
    // {
    //     $a = 4;
    //     $b = 0;
    //     $this->expectException(\DivisionByZeroError::class);
    //     $this->calculator->divide($a,$b);

        
    // }
}
