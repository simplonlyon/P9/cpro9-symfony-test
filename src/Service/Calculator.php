<?php

namespace App\Service;

class Calculator {

    public function add(float $a, float $b): float {
        return  $a + $b;
    }
    
    public function substract(float $a, float $b): float {
        return  $a - $b;
    }
    
    public function multiply(float $a, float $b): float {
        return  $a * $b;
    }

    public function divide(float $a, float $b): float {
        // if($b == 0) {
        //     throw new \DivisionByZeroError();
        // }
        return  $a / $b;
    }

    public function isEven($value): bool {
        if(!is_numeric($value)) {
            throw new \InvalidArgumentException();
        }
        return $value % 2 == 0;
    }

}