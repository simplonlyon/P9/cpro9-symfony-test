<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;

class ExampleTest extends TestCase
{
    public function testSomething()
    {
        $tableau = ["name" => "Jean", "surname" => "Demel"];

        $this->assertArrayHasKey("name", $tableau);
    }
}
