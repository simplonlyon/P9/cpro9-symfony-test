<?php

namespace App\Tests\Repository;

use App\Entity\Dog;
use App\Repository\DogRepository;
use DateTime;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DogRepositoryTest extends KernelTestCase
{

    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var DogRepository
     */
    private $repo;

    public function setUp() {
        $kernel = self::bootKernel();

        $this->em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $this->repo = $this->em
            ->getRepository(Dog::class);
    }

    public function testFindAll() {
        $results = $this->repo->findAll();

        $this->assertEquals(4, count($results));
        $first = $results[0];

        $this->assertEquals('Dog1', $first->getName());
        $this->assertEquals('Breed1', $first->getBreed());
    }

    public function testFindOne() {
        
        $result = $this->repo->find(1);

        $this->assertInstanceOf(Dog::class, $result);
        

        $this->assertEquals('Dog1', $result->getName());
        $this->assertEquals('Breed1', $result->getBreed());
    }

    public function testAdd() {
        $dog = new Dog();
        $dog->setName("test");
        $dog->setBreed("test");
        $dog->setBirthdate(new DateTime("2020-03-02"));

        $this->em->persist($dog);
        $this->em->flush();

        $this->assertNotNull($dog->getId());

    }

    public function testUpdate() {
        
        $dog = $this->repo->find(1);
        $dog->setBreed("test");       
        
        $this->em->flush();

        $dogDb = $this->repo->find(1);
        $this->assertEquals("test", $dogDb->getBreed());
        
    }

    public function testDelete() {
        
        $dog = $this->repo->find(1);
        

        $this->em->remove($dog);
        $this->em->flush();

        $results = $this->repo->findAll();
        $this->assertEquals(3, count($results));
        $this->assertEquals(2, $results[0]->getId());
        
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->em->close();
        $this->em = null;
        $this->repo = null;
    }
}
